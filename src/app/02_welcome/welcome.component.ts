import { Component, OnInit, Injectable } from '@angular/core';
import { Constants } from '../constant';
import { Router } from '@angular/router';

import { Address, Location, Course, CourseEvent, User } from '../00_commons/interface';
import { VariableSetup } from '../00_commons/setup-constans';
import { UserService } from '../services/user.service';
import { NewTrainingService } from '../services/getlist.training-new.service';

@Component({
	selector: 'welcome',
	templateUrl: './welcome.component.html',
	styleUrls: ['../../../assets/css/welcome.component.css']
})
export class WelcomeComponent {
	guestList: User[];
	courseList: Course[];
	currentUser: User;
	actClients: number;
	inquirer: number;
    clientPageCount: number;
    coursePageCount: number;
	currentClientPage: number;
	currentCoursePage: number;
    clientPages: number[];
    coursePages: number[];
    itemsPerPage: any[];
    currentItemsPerClientPage: number;
    currentItemsPerCoursePage: number;


	constructor( private userService:UserService, private router:Router, private trainingService:NewTrainingService) {
		this.guestList = [];
		this.courseList = [];
		this.currentUser = {
			name: "",
			email: "",
            isActive: false,
			id: null,
            role: ""
		};
		this.actClients = 0;
		this.inquirer = 0;
        this.clientPageCount = 0;
        this.coursePageCount = 0;
		this.currentClientPage = 1;
		this.currentCoursePage = 1;
        this.clientPages = [];
        this.coursePages = [];
        this.itemsPerPage = VariableSetup.getItemsPerPageNumbers();
        this.currentItemsPerClientPage = this.itemsPerPage[VariableSetup.defaultItemsPerPageNumbersIndex].key;   
        this.currentItemsPerCoursePage = this.itemsPerPage[VariableSetup.defaultItemsPerPageNumbersIndex].key;   
	}

	ngOnInit () {
		document.body.className = "";
/*		this.userService.getEveryUserList().then(users => this.guestList = users);
        this.userService.getCurrentUser().then( user => this.currentUser = user);*/
	}

    pagesList ( pagesCount : number) : number[] {
        let pages : number[] = [];
        for (let i = 1; i <= pagesCount; i++) {
            pages.push(i);
        }
        return pages;
    }

	getNeededClientPage( pageNo : number) : void {
		if ( pageNo>=1 && pageNo<=this.clientPageCount ) {
			this.userService.getClientsToDashboard(this.currentClientPage, this.currentItemsPerClientPage).then(result => {
				this.guestList = result.users;		
				this.clientPageCount = result.pageCount;
			});
			this.currentClientPage = pageNo;
		}
	}

    getNeededCoursePage( pageNo : number) : void {
        if ( pageNo>=1 && pageNo<=this.coursePageCount ) {
            this.userService.getCoursesToDashboard(this.currentCoursePage, this.currentItemsPerCoursePage).then(result => {
                this.courseList = result.courses;        
                this.coursePageCount = result.pageCount;
            });
            this.currentCoursePage = pageNo;
        }
    }

    navigateToClient(userId : number) : void {
        this.router.navigate(['/profile', userId]);    //ilyenkor a /profile után egy per jel után az id-t hozzáadja
    }

    navigateToCourse(courseId : number) : void {
        this.router.navigate(['/course', courseId]);    //ilyenkor a /profile után egy per jel után az id-t hozzáadja
    }

    hasRole( roleName : string) : boolean {
        let t : boolean = false;
        for (let role of this.currentUser.role) {
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    }

}