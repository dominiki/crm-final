import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {NewTrainingService} from'../services/getlist.training-new.service';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../constant';
import { UserService } from '../services/user.service';
import { URLSearchParams } from "@angular/http";
import { Course, CourseEvent, User, Location, Discount, Address} from '../00_commons/interface';

declare var $ : any;

@Component({
 selector: 'training-new',
  //selector: 'trainingDetails',
  templateUrl: './trainingDetails.component.html',
  providers: [NewTrainingService]
})
export class TrainingDetails { 


  training : CourseEvent;
  course: Course;
  user: User;
  location : Location;
  address: Address;
  //genders : KeyValueObj[];

  errors : any;
  errorText: string;
  postText: string;
  sub : any;
  id : string;
  trainingDateStartDate: string;
  trainingDateFinishTime: string;
  courseTypes: Array<Object>;
  courseIdGet: Array<Object>;
  trainersList : Array<Object>;
  courseEvents : Array<string>;
  trainerNameGet : Array<string>;
  addCourseType : Array<string>;
  backCopy : Array<Object>;
 
  newTrainingServiceInstance: NewTrainingService;
  // itt vannak a promisok

  //@Input()
    trainingEvents : Array<CourseEvent>=[];

  constructor (private newTrainingService: NewTrainingService, private userService:UserService, private activatedRoute : ActivatedRoute) {

    
    this.address = {
      zip: null,
      city: "",
      street: "",
      number: ""
    }

    this.location = {
      address : this.address,
      room : ""
    }

    this.training = {
      name: "",
      eventId: null,
      description: "", 
      teachers: [],
      location: this.location,
      dateStart: "",
      dateFinish: ""
    }




    this.errorText = "";
   // this.genders = genders;
    this.errors = {};
    this.newTrainingServiceInstance = newTrainingService;
    this.postText = "";
    this.trainingDateStartDate="";
    this.trainingDateFinishTime="";
    this.sub = "";
    this.id ="";
    this.courseTypes = new Array<Object>();
    this.trainersList = new Array<Object>();
    this.courseEvents = new Array<string>();
    this.courseIdGet = new Array<Object>();
    this.addCourseType= new Array<string>();
    this.backCopy = new Array<Object>();
   

    this.course = {

      courseName: "",
      courseDescription: "",
      courseId: this.activatedRoute.snapshot.params['course.id'],
      applicationDeadline: "",
      maxStudents: null,
      minStudents: null,
      courseDateStart: "",  
      courseDateFinish: "", 
      discount: [],
      price: null,
      priceDeadline: "",
      events: this.trainingEvents,
     

    }


  }

	ngOnInit () :void {
    this.sub = this.activatedRoute.params.subscribe(course => {
       this.id = course['course.courseId'];
    });
    console.log(this.id);
		document.body.className = "page-container-bg-solid";
        $( "#datepicker" ).datepicker({ 
            dateFormat: 'yy-mm-dd', 
            yearRange: '1910:2017',
            changeYear: true   
        });   
        this.getTypes();
        this.getTrainers();
        this.getCourseById();

        /* console.log( this.activatedRoute.snapshot.params['course.id']);
        this.newTrainingService.getTrainingData(this.user.id)
        .then( course => this.course = course );*/
     

	}

  addCourseEvents() : void {
    if(this.training.teachers == []){
     let nullTeacher=Array<User>();
     nullTeacher.push(  );
    }
    let trainingCopy =JSON.parse(JSON.stringify(this.training))
    this.trainingEvents.push(trainingCopy);
  } 

  deleteCourseEvents(index: number): void{
    this.trainingEvents.splice(index, 1);
  }

  getTypes () :void {
    this.newTrainingServiceInstance.listTypes()
    .then( (response)=>{
        this.courseTypes = response.json();
      })
      .catch( ( response )=>{
        console.log(response);
        this.errorText = "Hiba van a types listában!";
      });

  }

  getTrainers (): void {
    this.newTrainingServiceInstance.listGetTrainers()
    .then( (trainers)=>{
      this.trainersList = trainers.json();
      this.trainerNameGet = trainers.json();
    })
    .catch( ( trainers ) =>{
      this.errorText = "Hiba van a trainers litában"
    })
  }

  trainingRequest() :void {

/*
    let courseEvents= new Array<Object>();
    this.course.events.forEach((event) => {
      let trainers = new Array<string>();
      event.teachers.forEach((teacher) => {
         trainers.push(teacher.id); 
      });
      let typespush = new Array<string>();
      this.course.types.forEach((types) =>{
          typespush.push(types);
          });
      

      courseEvents.push({
        name: event.name,
        courseEventDescription: event.description,
        dateStart: event.dateStart,
        dateFinish: event.dateFinish,
        trainers: trainers,
        location: event.location
      });
      
    });
    let requestObj = {
      name: this.course.courseName,
      description: this.course.courseDescription,
      discount: this.course.discount,
      maxStudents: ""+this.course.maxStudents,
      minStudents: ""+this.course.minStudents,
      applicationDeadline: this.course.applicationDeadline,
      price: ""+this.course.price,
      courseEvents: courseEvents
     };
   

    this.newTrainingServiceInstance.postTrainingRequestPut (requestObj)
    .then(response =>
      alert("A " + this.course.courseName+" kurzus sikeresen rögzítve lett!")   
    )
    .catch( ( response )=>{
      try{
        console.log(response);
        let json = response.json();
        this.errorText = "Hiba van a trainer post-ban!";
      }
      catch (e) {
      this.errorText = "Hiba van a trainer post-ban!";
      }

    });*/
  }


 // getCourseById( this.activatedRoute.snapshot.params['course.id']) : void {
  getCourseById() : void {
   
    this.newTrainingServiceInstance.listId(this.id)
        .then( (response)=>{
        this.courseIdGet = response.json();
        this.backCopy = this.courseIdGet;
      })
      .catch( ( response )=>{
        console.log(response);
        this.errorText = "Hiba van a types listában!";
      });
      
  }

addCourseTypeButton() : void {
    let coursePush = new Array<Object>();
     coursePush.push({
      text : this.addCourseType,

    });
    this.newTrainingServiceInstance.courseTypePush(coursePush)
    .then( ()=>{
      //this.router.navigate(['dashboard']);
      this.getTypes ();
      })
    .catch( ( response )=>{
      try{
        console.log(response);
        let json = response.json();
        this.errorText = "Hiba van a trainer post-ban!";
      }
      catch (e) {
      this.errorText = "Hiba van a trainer post-ban!";
      }

    });

  }

  backToCourseEvent(): void{
    $("html, body").animate({ scrollTop: $(document).height()-$(window).height() },'slow');
  }

  backToCourse(): void {
    $("html, body").animate({ scrollTop: "260px" }, 'slow');
  }

}