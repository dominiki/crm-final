import { Injectable }    from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class MessageService {

	private subject: Subject<any>;

	constructor() {
		this.subject = new Subject<any>();
	}

    sendMessage(title : string, message: string) {
        this.subject.next({ title, message });
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

}