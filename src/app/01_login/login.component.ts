import { Component, OnInit, Injectable } from '@angular/core';
import { Http,  HttpModule, Response, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../constant';
import { UserService } from '../services/user.service';
import { User } from '../00_commons/interface';


@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
 	
	username: string;
	password: string;
	errorText: string;

	
	constructor (private http: Http, private router: Router, private userService:UserService ) {
		this.username = "";
		this.password = "";
		this.errorText = "";
	}

	ngOnInit () {
		document.body.className = "login"; 
	}

	doLogin(): void {
		this.errorText = '';
		if (this.username == '') {
			this.errorText = 'Adja meg a felhasználónevét!';
			return;
		} else if (this.password == '') {
			this.errorText = 'Adja meg a jelszavát!';
			return;
		}else{
			let loginResult : number = this.userService.logIn(this.username, this.password);
			if (loginResult == 0) {
				this.router.navigate(['dashboard']);
			} else if (loginResult == 1){
				this.errorText = "Érvénytelen felhasználónév vagy jelszó!";
			} else {
				this.errorText = "Hálózati hiba!";
			};
		};
	};

}