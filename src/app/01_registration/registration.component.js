"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var setup_constans_1 = require("../00_commons/setup-constans");
var constant_1 = require("../constant");
var user_service_1 = require("../services/user.service");
var message_service_1 = require("../services/message.service");
require("rxjs/add/operator/toPromise");
var RegistrationComponent = (function () {
    function RegistrationComponent(http, applicationRef, messageService, userService, router) {
        this.http = http;
        this.applicationRef = applicationRef;
        this.messageService = messageService;
        this.userService = userService;
        this.router = router;
        this.user = {
            email: "",
            name: "",
            isActive: false,
            role: ""
        };
        this.roles = "";
        this.invExp = setup_constans_1.VariableSetup.getInvExp();
        this.invText = setup_constans_1.VariableSetup.getInvText();
        this.errors = {};
    }
    RegistrationComponent.prototype.ngOnInit = function () {
        document.body.className = "profile";
        $('#registrerSuccessModal').modal({ show: false });
    };
    RegistrationComponent.prototype.doSubmit = function () {
        var _this = this;
        this.errors = {};
        if (!this.userService.wordLengthValidator(this.user.name, 1)) {
            this.errors.name = 'Kérem adja meg a nevét!';
        }
        if (!this.userService.emailValidator(this.user.email)) {
            this.errors.email = 'Érvénytelen email formátum!';
        }
        if (!this.user.role[0]) {
            this.errors.role = 'Kérem adja meg az ügyfél típusát!';
        }
        if (!this.userService.wordLengthValidator(this.invExp + "", 1)) {
            this.errors.invExp = 'Kérem adja meg a meghívó érvényességét!';
        }
        if (Object.keys(this.errors).length == 0) {
            var data = {
                name: this.user.name,
                email: this.user.email,
                role: this.user.role,
                text: this.invText,
                expiry: this.invExp
            };
            this.http.post(constant_1.Constants.BASEURL + 'rest/invitation', data, { withCredentials: true })
                .toPromise()
                .then(function (response) {
                var json = response.json();
                if (json.status == "ok") {
                    $('#registrerSuccessModal').modal('show').on('hidden.bs.modal', function () {
                        _this.router.navigate(['dashboard']);
                    });
                }
            })
                .catch(function (response) {
                _this.messageService.sendMessage("Szerverhiba", "A szerver nem várt választ adott: " + response.error);
            });
        }
        else {
            this.applicationRef.tick();
            var formGroup = document.getElementsByClassName("form-group");
            for (var i = 0; formGroup.length > i; i++) {
                if (formGroup[i].classList.contains("has-error")) {
                    var rect = formGroup[i].getBoundingClientRect();
                    window.scrollBy(0, rect.top);
                    break;
                }
            }
            ;
        }
    };
    RegistrationComponent.prototype.onBlurMethodName = function () {
        if (this.userService.wordLengthValidator(this.user.name, 1)) {
            delete this.errors.name;
        }
    };
    RegistrationComponent.prototype.onBlurMethodEmail = function () {
        if (this.userService.emailValidator(this.user.email)) {
            delete this.errors.email;
        }
    };
    RegistrationComponent.prototype.onBlurMethodRole = function () {
        if (this.userService.wordLengthValidator(this.user.role[0], 1)) {
            delete this.errors.role;
        }
    };
    RegistrationComponent.prototype.onBlurMethodInvExp = function () {
        if (this.user.role[0]) {
            delete this.errors.invExp;
        }
    };
    return RegistrationComponent;
}());
RegistrationComponent = __decorate([
    core_1.Component({
        selector: 'registration',
        templateUrl: './registration.component.html'
    }),
    __metadata("design:paramtypes", [http_1.Http, core_1.ApplicationRef, message_service_1.MessageService, user_service_1.UserService, router_1.Router])
], RegistrationComponent);
exports.RegistrationComponent = RegistrationComponent;
//# sourceMappingURL=registration.component.js.map