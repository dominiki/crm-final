import { Component, Injectable } from '@angular/core';
import { MessageService } from '../services/message.service';
import { Subscription } from 'rxjs/Subscription';

declare var $ : any;

@Component({
  selector: 'crm-footer',
  templateUrl: './crm-footer.component.html'
})
export class FooterComponent {
	errorMessage: string;
	errorTitle : string;
    subscription: Subscription;

    constructor(private messageService: MessageService) {
        $('#errorModal').modal({ show: false });
        this.subscription = this.messageService.getMessage().subscribe(obj => {
        	this.errorMessage = obj.message;
        	this.errorTitle = obj.title;
        	$('#errorModal').modal('show');
        });
    }


}

