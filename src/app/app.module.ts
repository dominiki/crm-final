import { NgModule, ErrorHandler }      from '@angular/core';
import { FormsModule }	from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';

import { routes }  from './app.routing';

import { TablePagerComponent } from './00_tablepager/tablepager.component';

import { LoginComponent } from './01_login/login.component';
import { ForgottenPwComponent } from './01_forgottenpassword/forgotten-pw.component';
import { RegistrationComponent } from './01_registration/registration.component';
import { RegistrationComponent2 } from './01_registration/registration2.component';
import { SuccessReg } from './01_registration/success-reg.component';
import { TrainingNewComponent } from './02_training_new/training-new.component';
import { UsersComponent } from './02_users/users.component';
import { ClientDetailsComponent } from './02_clientDetails/clientDetails.component';
import { ClientsComponent } from './02_clients/clients.component';
import { SetupComponent } from './02_setup/setup.component';
import { DashboardComponent } from './02_dashboard/dashboard.component';
import { FooterComponent } from './02_crm-footer/crm-footer.component';
import { HeaderComponent } from './02_crm-header/crm-header.component';
import { AppComponent }  from './app.component';
import { ProfileComponent } from './03_profile/profile.component';
import { WelcomeComponent } from './02_welcome/welcome.component';
import { MapComponent } from './03_map/map.component';
import { TrainingsComponent } from './02_trainings/trainings.component'
import { UserService } from './services/user.service';
import { MessageService } from './services/message.service';
import { NewTrainingService } from './services/getlist.training-new.service';
import { UserDetailsComponent } from './03_UserDetails/userDetails.component';
import { TrainingDetails } from './02_trainingDetails/trainingDetails.component';

import { LocationPipe } from './pipes/location.pipe';
import { AddressPipe } from './pipes/address.pipe';

@NgModule({
  imports: 	[ BrowserModule, FormsModule, routes, HttpModule ],

  
  declarations: [ 	AppComponent, 
  					TablePagerComponent,
					ForgottenPwComponent, 
					LoginComponent, 
					RegistrationComponent, 
					RegistrationComponent2,
					SuccessReg, 
					TrainingNewComponent, 
					DashboardComponent, 
					FooterComponent, 
					HeaderComponent, 
					ProfileComponent, 
					UsersComponent, 
					ClientsComponent,
					SetupComponent,
					WelcomeComponent,
					MapComponent,
					TrainingsComponent,
					AddressPipe,
					LocationPipe,
					ClientDetailsComponent,
					UserDetailsComponent,
					TrainingDetails],
  bootstrap:    [ AppComponent ],


  providers: [UserService, NewTrainingService, MessageService]

  
})
export class AppModule { }
