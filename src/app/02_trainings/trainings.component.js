"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var getlist_training_new_service_1 = require("../services/getlist.training-new.service");
require("rxjs/add/operator/toPromise");
var TrainingsComponent = (function () {
    function TrainingsComponent(courseService, router, activatedRoute) {
        this.courseService = courseService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.simpleFilterData = "";
        this.filterData = {
            name: '',
            type: '',
            dateStart: '',
            dateFinish: '',
            location: '',
            id: ""
        };
        this.allCourses = [];
    }
    TrainingsComponent.prototype.ngOnInit = function () {
        document.body.className = "container-fluid";
        $("#datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
    };
    TrainingsComponent.prototype.simpleSearch = function () {
        var _this = this;
        this.courseService.getFilteredCourseList(this.simpleFilterData).then(function (courses) { return _this.allCourses = courses; });
    };
    TrainingsComponent.prototype.searchCourse = function () {
        var _this = this;
        var filterString = '';
        if (this.filterData.name != "") {
            filterString = this.filterData.name;
        }
        else if (this.filterData.type != "") {
            filterString = this.filterData.type;
        }
        else if (this.filterData.startDate != "") {
            filterString = this.filterData.startDate;
        }
        else if (this.filterData.location != "") {
            filterString = this.filterData.location;
        }
        this.courseService.getFilteredCourseList(this.filterData).then(function (courses) { return _this.allCourses = courses; });
    };
    TrainingsComponent.prototype.complexSearch = function () {
        this.courseService.getcomplexSearch();
    };
    return TrainingsComponent;
}());
TrainingsComponent = __decorate([
    core_1.Component({
        selector: 'trainings',
        templateUrl: './trainings.component.html'
    }),
    __metadata("design:paramtypes", [getlist_training_new_service_1.NewTrainingService, router_1.Router, router_1.ActivatedRoute])
], TrainingsComponent);
exports.TrainingsComponent = TrainingsComponent;
//# sourceMappingURL=trainings.component.js.map