"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var setup_constans_1 = require("../00_commons/setup-constans");
var TablePagerComponent = (function () {
    function TablePagerComponent() {
        this.rowClick = new core_1.EventEmitter();
        this.pageCount = 0;
        this.currentPage = 1;
        this.pages = [];
        this.itemsPerPage = setup_constans_1.VariableSetup.getItemsPerPageNumbers();
        this.currentItemsPerPage = this.itemsPerPage[setup_constans_1.VariableSetup.defaultItemsPerPageNumbersIndex].key;
        this.tableData = [];
        this.tableHeaders = [];
    }
    TablePagerComponent.prototype.ngOnInit = function () {
        this.startSearch();
    };
    TablePagerComponent.prototype.startSearch = function () {
        var _this = this;
        this.searchFunction.apply(this.owner, [this.searchFilter, this.currentPage, this.currentItemsPerPage]).then(function (result) {
            _this.pageCount = result.pageCount;
            _this.generatePages();
            _this.tableData = result;
        });
    };
    TablePagerComponent.prototype.generatePages = function () {
        this.pages = [];
        for (var i = 1; i <= this.pageCount; i++) {
            this.pages.push(i);
        }
    };
    TablePagerComponent.prototype.setPage = function (pageNo) {
        if (pageNo >= 1 && pageNo <= this.pageCount) {
            this.currentPage = pageNo;
            this.startSearch();
        }
    };
    TablePagerComponent.prototype.emitRowClick = function (data) {
        this.rowClick.emit(data);
    };
    return TablePagerComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], TablePagerComponent.prototype, "searchFilter", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Function)
], TablePagerComponent.prototype, "searchFunction", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], TablePagerComponent.prototype, "emptyResultText", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], TablePagerComponent.prototype, "tableHeaders", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", core_1.Component)
], TablePagerComponent.prototype, "owner", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], TablePagerComponent.prototype, "rowClick", void 0);
TablePagerComponent = __decorate([
    core_1.Component({
        selector: 'table-pager',
        templateUrl: './tablepager.component.html',
    }),
    __metadata("design:paramtypes", [])
], TablePagerComponent);
exports.TablePagerComponent = TablePagerComponent;
//# sourceMappingURL=tablepager.component.js.map