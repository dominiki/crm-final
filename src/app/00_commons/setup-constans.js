"use strict";
var DEFAULT_INVITATION_EXP = 7;
var DEFAULT_INVITATION_TEXT = "Az alábbi meghívóval kérem regisztráljon:";
var VariableSetup = (function () {
    function VariableSetup() {
    }
    VariableSetup.getInvExp = function () {
        return DEFAULT_INVITATION_EXP;
    };
    VariableSetup.getInvText = function () {
        return DEFAULT_INVITATION_TEXT;
    };
    VariableSetup.getIssueText = function () {
        return "Személyes azonosításra alkalmas adatokon, információn azokat a természetes személyekre vonatkozó személyes adatokat értjük, melyek segítségével valakit személyében azonosítani lehet, valakivel kommunikációs kapcsolatba lehet kerülni, vagy valakinek meg lehet határozni a fizikai elérhetőségét - ideértve, de nem korlátozva az alábbiakra: név, lakáscím, postacím, telefonszám, faxszám, e-mail cím, banki minősítés, társadalombiztosítási szám, adóhatósági azonosító, hitelkártya információ, ügyfélprofilok, biometrikus azonosítók. Személyes azonosításra alkalmas adatokon, információn azokat a természetes személyekre vonatkozó személyes adatokat értjük, melyek segítségével valakit személyében azonosítani lehet, valakivel kommunikációs kapcsolatba lehet kerülni, vagy valakinek meg lehet határozni a fizikai elérhetőségét - ideértve, de nem korlátozva az alábbiakra: név, lakáscím, postacím, telefonszám, faxszám, e-mail cím, banki minősítés, társadalombiztosítási szám, adóhatósági azonosító, hitelkártya információ, ügyfélprofilok, biometrikus azonosítók. Személyes azonosításra alkalmas adatokon, információn azokat a természetes személyekre vonatkozó személyes adatokat értjük, melyek segítségével valakit személyében azonosítani lehet, valakivel kommunikációs kapcsolatba lehet kerülni, vagy valakinek meg lehet határozni a fizikai elérhetőségét - ideértve, de nem korlátozva az alábbiakra: név, lakáscím, postacím, telefonszám, faxszám, e-mail cím, banki minősítés, társadalombiztosítási szám, adóhatósági azonosító, hitelkártya információ, ügyfélprofilok, biometrikus azonosítók.";
    };
    VariableSetup.getItemsPerPageNumbers = function () {
        return [{ key: 5, text: "5" }, { key: 10, text: "10" }, { key: 25, text: "25" }, { key: 0, text: "Mind" }];
    };
    return VariableSetup;
}());
VariableSetup.defaultItemsPerPageNumbersIndex = 0;
exports.VariableSetup = VariableSetup;
//# sourceMappingURL=setup-constans.js.map