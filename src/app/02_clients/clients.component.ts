import { Component, OnInit, Injectable } from '@angular/core';

import { Address, Location, Course, CourseEvent, User, SearchFilter, ClientSimpleSearchFilter, ClientCombinedSearchFilter } from '../00_commons/interface';
import { VariableSetup } from '../00_commons/setup-constans';
import { UserService }	from '../services/user.service';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';

@Component({
	selector: 'clients',
	templateUrl: './clients.component.html',
	
})

export class ClientsComponent implements OnInit{

    simplySearch: boolean;    //ez kell az egyszerű/összetett keresés váltáshoz
	filterDataCombined : ClientCombinedSearchFilter;    //keresési feltételek
    filterDataSimple : ClientSimpleSearchFilter;    //egyszerű keresési feltétel
    lastFilterData : SearchFilter;    //utoljára elküldött összetett keresési feltétel

	currentUser: User;        //bejelentkezett user
	status: string[];

	constructor(private userService:UserService, private router:Router) {
		this.filterDataCombined = {
            name : '',
            email : '',
            status : '',
        };
        this.filterDataSimple = {
            s : '',
        }
        this.simplySearch = true;
        this.lastFilterData = this.filterDataSimple;
        this.currentUser = {
			name: "",
			email: "",
            isActive: false,
			id: null,
			status: ""
		};
		this.status = [];       
	}

	ngOnInit () : void {
		this.userService.getStatus().then( status => { this.status = status });
//        this.userService.getCurrentUser().then( user => this.currentUser = user);
	}

    saveSimple() : void {
        this.lastFilterData = this.filterDataSimple;
    }

    saveCombined() : void {
        this.lastFilterData = this.filterDataCombined;
    }

    doClientSearch(searchFilter : SearchFilter, page: number, itemsPerPage : number) : Promise<any> {
        return (this.lastFilterData['s'] !== undefined) ? this.simpleSearchUser(searchFilter, page, itemsPerPage) : this.combinedSearchUser(searchFilter, page, itemsPerPage);
    }

	simpleSearchUser(searchFilter : SearchFilter, page: number, itemsPerPage : number) : Promise<any> {
		return this.userService.getUsersSimpleSearch(searchFilter as ClientSimpleSearchFilter, page, itemsPerPage);
	}

	combinedSearchUser(searchFilter : SearchFilter, page: number, itemsPerPage : number) : Promise<any> {
		return this.userService.getUsersCombinedSearch(searchFilter as ClientCombinedSearchFilter, page, itemsPerPage);
	}

	navigateToUser(data : any) : void {
        let userId = data.id;
		this.router.navigate(['/user', userId]);	//ilyenkor a /profile után egy per jel után az id-t hozzáadja
	}

	changeSearch() : void {
		this.simplySearch = !this.simplySearch;
	}

	hasRole( roleName : string) : boolean {
        let t : boolean = false;
        for (let role of this.currentUser.role) {
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    }

}	