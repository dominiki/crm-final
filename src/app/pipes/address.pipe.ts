import { Pipe, PipeTransform } from '@angular/core';
import { Address }  from '../00_commons/interface';

@Pipe ({

	name : 'addressPipe'

})
export class AddressPipe implements PipeTransform {

	transform( loc : Address ) {
		return loc.zip + ' ' + loc.city + ', ' + loc.street + ' ' + loc.number;
	}
}