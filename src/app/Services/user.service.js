"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
require("rxjs/add/operator/toPromise");
var UserService = (function () {
    function UserService(http, router) {
        this.http = http;
        this.router = router;
        this.createProfileURL = "rest/client";
        this.defaultUser = {
            name: "John Doe",
            email: "johndoe@dummyemail.com",
            isActive: true,
            id: 10,
            username: "a",
            password: "a",
            role: "Adminisztrátor"
        };
        this.jsonLink = "../../assets/json/";
    }
    UserService.prototype.logIn = function (user, pass) {
        var _this = this;
        //		var accessResult : number = 1;
        if (user == this.defaultUser.username && pass == this.defaultUser.password) {
            this.currentUser = this.defaultUser;
            return 0;
        }
        else {
            this.getUsersFromJSON()
                .then(function (userList) {
                console.log(userList.length);
                for (var _i = 0, userList_1 = userList; _i < userList_1.length; _i++) {
                    var u = userList_1[_i];
                    if (user == u.username && pass == u.password) {
                        console.log(u.username);
                        _this.currentUser = u;
                        console.log(_this.currentUser);
                    }
                }
                if (!_this.currentUser) {
                }
            })
                .catch(function () { });
        }
    };
    UserService.prototype.getUsersFromJSON = function () {
        return this.http.get(this.jsonLink + "users.json", { withCredentials: true })
            .toPromise()
            .then(function (response) {
            return response.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.getCurrentUser = function () {
        return this.defaultUser;
        /*		if (this.currentUser) {
                    return new Promise((resolve, reject) => {
                        resolve(this.currentUser);
                    });
                    
                } else {
                    return this.http.get(
                        Constants.BASEURL + "rest/users/current",
                        {withCredentials: true}
                    )
                    .toPromise()
                    .then(( response ) => {
                        this.currentUser = response.json();
                        return response.json() as User;
                    } )
                    .catch( this.handleError );
                }*/
    };
    UserService.prototype.logOut = function () {
        return null;
        /*		return this.http.get(
                    Constants.ROOTURL+"logout",
                    {withCredentials: true})
                .toPromise();	*/
    };
    UserService.prototype.getStatus = function () {
        return null;
        /*		return this.http.get(
                    Constants.BASEURL+"rest/enum/status",
                    {withCredentials: true})
                .toPromise()
                .then( (response) => {
                    return response.json() as string[];
                })
                .catch(this.handleError);*/
    };
    UserService.prototype.getUsersSimpleSearch = function (searchFilter, page, itemsPerPage) {
        return null;
        //		return this.http.get('../../assets/json/dummy_user_pager.json')
        /*		return this.http.get(
                    Constants.BASEURL+"rest/client?searchexpression="+ searchFilter.s
                        +"&pagenumber="+ page +"&maxperpage=" + itemsPerPage,
                    {withCredentials: true}
                )
                .toPromise()
                .then( (response) => {
                    return response.json() as UserSearch;
                })
                .catch(this.handleError);*/
    };
    UserService.prototype.getUsersCombinedSearch = function (searchFilter, page, itemsPerPage) {
        return null;
        //		return this.http.get('../../assets/json/dummy_user_pager.json')
        /*		return this.http.get(
                    Constants.BASEURL+"rest/findclient?name="+ searchFilter.name +"&email="+ searchFilter.email
                        +"&status="+ searchFilter.status +"&pagenumber="+ page +"&maxperpage=" + itemsPerPage,
                    {withCredentials: true}
                )
                .toPromise()
                .then( (response) => {
                    return response.json() as UserSearch;
                })
                .catch(this.handleError);*/
    };
    UserService.prototype.getClientsToDashboard = function (clickedP, itemPP) {
        return null;
        /*		return this.http.get(
                    Constants.BASEURL+"rest/dashboard/clients?pagenumber=<" + clickedP + ">&maxperpage=<" + itemPP + ">",
                    {withCredentials: true})
                .toPromise()
                .then( (response) => {
                    return response.json() as UserSearch;
                })
                .catch(this.handleError);*/
    };
    UserService.prototype.getCoursesToDashboard = function (clickedP, itemPP) {
        return null;
        /*		return this.http.get(
                    Constants.BASEURL+"rest/dashboard/courses?pagenumber=<" + clickedP + ">&maxperpage=<" + itemPP + ">",
                    {withCredentials: true})
                .toPromise()
                .then( (response) => {
                    return response.json() as CourseSearch;
                })
                .catch(this.handleError);*/
    };
    UserService.prototype.userFromRegistrationId = function (idText) {
        return null;
        /*		return this.http.get(
                    Constants.BASEURL+"rest/register?id=" + idText,
                    {withCredentials: true})
                .toPromise()
                .then( (response) => {
                    let json = response.json();
                    if (json.errorCode && json.errorCode == "NotValidRegUrl") {
                        return new Promise<RegistrationStep2Response>((resolve, reject) => { reject(); });
                    } else {
                        return json as RegistrationStep2Response;
                    }
                })
                .catch(this.handleError);*/
    };
    UserService.prototype.wordLengthValidator = function (word, wordLength) {
        return word.length >= wordLength;
    };
    UserService.prototype.phoneNumberValidator = function (phoneNumber) {
        if (phoneNumber.length < 8) {
            return false;
        }
        var regexpPhone = /^\+[\d]*$/;
        return regexpPhone.test(phoneNumber);
    };
    UserService.prototype.emailValidator = function (email) {
        if (email.length < 9) {
            return false;
        }
        if (email.charAt(0) == "." || email.charAt(0) == "@" || email.charAt(email.length - 1) == "." || email.charAt(email.length - 1) == "@") {
            return false;
        }
        var atChar = false;
        var dotChar = false;
        var atIndex;
        for (var i = 1; i < email.length - 1; i++) {
            if (email.charAt(i) == "@" && !atChar) {
                atChar = true;
                atIndex = i;
            }
            else if (atChar && email.charAt(i) == "@") {
                return false;
            }
            else if (atChar && i - atIndex > 1 && email.charAt(i) == ".") {
                dotChar = true;
            }
        }
        return atChar && dotChar;
    };
    UserService.prototype.passwordValidator1 = function (pass) {
        if (pass.length < 6) {
            return false;
        }
        else {
            var smallChar = false;
            var bigChar = false;
            var numberChar = false;
            var regexpS = /[a-z]/;
            var regexpB = /[A-Z]/;
            var regexpN = /\d/;
            for (var i = 0; i < pass.length; i++) {
                if (regexpS.test(pass.charAt(i))) {
                    smallChar = true;
                }
                else if (regexpB.test(pass.charAt(i))) {
                    bigChar = true;
                }
                else if (regexpN.test(pass.charAt(i))) {
                    numberChar = true;
                }
                if (smallChar && bigChar && numberChar) {
                    return true;
                }
            }
            return false;
        }
    };
    UserService.prototype.getGuestList = function () {
        return null;
        /*		return this.http.get(
                    Constants.BASEURL+"rest/client/all",
                    {withCredentials: true})
                .toPromise()
                .then((response)=>{
                    return response.json() as User[];
                })
                .catch( this.handleError );*/
    };
    UserService.prototype.handleError = function (error) {
        return null;
        /*	    console.error('HIBA', error.status); // for demo purposes only
                return Promise.reject(error.message || error);*/
    };
    UserService.prototype.getEveryUserList = function () {
        return null;
        /*		return this.http.get(
                    Constants.BASEURL+"rest/client/all",
                    {withCredentials: true})
                .toPromise()
                .then((response)=>{
                    return response.json() as User[];
                })
                .catch( this.handleError );*/
    };
    UserService.prototype.getFilteredUserList = function (searched) {
        return null;
        /*		return this.http.get(
                    Constants.BASEURL+"rest/client?searchexpression=" + searched,
                    {withCredentials: true})
                .toPromise()
                .then((response)=>{
                    return response.json() as User[];
                })
                .catch( this.handleError );*/
    };
    UserService.prototype.getFilteredUserListComplex = function (searched) {
        return null;
        /*		return this.http.get(
                    Constants.BASEURL+"rest/client?searchexpression=" + searched,
                    {withCredentials: true})
                .toPromise()
                .then((response)=>{
                    return response.json() as User[];
                })
                .catch( this.handleError );*/
    };
    UserService.prototype.createGuestProfile = function (guestProfile) {
        return null;
        /*    	return this.http.post(
                    Constants.BASEURL + this.createProfileURL, JSON.stringify(guestProfile),
                    {withCredentials: true})
                .toPromise()
                .then(res => res.json().data as string)
                .catch(this.handleError);*/
    };
    UserService.prototype.deleteGuest = function (id) {
        return null;
        /*  		return this.http.delete(
                    Constants.BASEURL +'rest/client?id=' + id,
                    {withCredentials: true})
                .toPromise()
                .then(() => {})
                .catch(this.handleError);*/
    };
    UserService.prototype.getGuestData = function (idText) {
        return null;
        /*		return this.http.get(
                    Constants.BASEURL+"rest/clientby?id=" + idText,
                    {withCredentials: true})
                .toPromise()
                .then( (response) => {
                    let json = response.json();
                    if (json.errorCode && json.errorCode == "NotValidRegUrl") {
                        return new Promise<User>((resolve, reject) => { reject(); });
                    } else {
                        return json as User;
                    }
                })
                .catch(this.handleError);*/
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map