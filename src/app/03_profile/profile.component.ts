import { Component, OnInit, NgModule, Injectable, ApplicationRef } from '@angular/core';
import { Http, HttpModule, Response, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';

import { Address, Location, Course, CourseEvent, genders, KeyValueObj, User } from '../00_commons/interface';
import { VariableSetup } from '../00_commons/setup-constans';
import { Constants } from '../constant';
import { UserService } from '../services/user.service';
import { NewTrainingService } from '../services/getlist.training-new.service';
import { MessageService } from '../services/message.service';
import 'rxjs/add/operator/toPromise';

declare var $ : any;

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html'
})
 
export class ProfileComponent {

	allCourses: Course[];
	user : User;
	genders : KeyValueObj[];
	locations : Location [];
	selectedLocations : any;
	errors : any;
	currentUser : User;
	status: string[];
	startDateContact : string;
	education: string;
	job: string;

	constructor( private http: Http, private router: Router, private userService:UserService, private courseService:NewTrainingService, private applicationRef : ApplicationRef, private messageService : MessageService ){
		
		$('#successModal').modal({ show: false });
		this.user = {
			name: "",
			email: "",
			isActive: false,
			gender: "",
			birthPlace: "",
			birthDate: "",
			phone: "",
			address : { zip : null, city : "", street : "", number : "", other: ""},
			role: "",
			otherInformation: "", 
			status: ""
	
		};
		this.status = [];
		this.startDateContact = "";
		this.education = "";
		this.job = "";
		this.currentUser = {
			name: "",
			email: "",
			isActive: false,
			id: null,
            role: ""
		};
		this.genders = genders;
		this.errors = {};
		this.allCourses = [];
		
	}

	ngOnInit() : void {
		document.body.className = "container-fluid";
		
		$( "#datepicker" ).datepicker({ 
			dateFormat: 'yy-mm-dd',
		    yearRange: '1910:2017',
		    changeYear: true }); 
		$( "#datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd'}); 
		this.userService.getStatus().then( status => this.status = status ); 
//		this.userService.getCurrentUser().then( user => this.currentUser = user);
	}
	navigate() {
		this.router.navigate(['dashboard']);
	}

	register() : void {
		
		this.errors = {};
		if (!this.userService.wordLengthValidator(this.user.name, 1)) {
			this.errors.name = 'Kérem adja meg a nevét!';
		}
		if (!this.userService.emailValidator(this.user.email)) {
			this.errors.email = 'Érvénytelen e-mail cím!';
		}
		if (!this.userService.phoneNumberValidator(this.user.phone)) {
			this.errors.phone = 'Érvénytelen telefonszám, kérem adja meg a telefonszámot a követekző módon: "+361234567"!';
		}
		if (!this.userService.wordLengthValidator(this.user.gender, 1)) {
			this.errors.gender = 'Kérem adja meg a nemét!';
		}

		if (Object.keys(this.errors).length == 0) {
		
			let json = {
				name : this.user.name,
				email : this.user.email,
				gender : this.user.gender,
				phone : this.user.phone,
				education : this.education,
				birthPlace : this.user.birthPlace,
				birthDate : this.user.birthDate,
				startDateContact : this.startDateContact,
				zip : this.user.address.zip,
				city : this.user.address.city,
				street : this.user.address.street,
				number : this.user.address.number,
				other : this.user.address.other,
				otherInformation : this.user.otherInformation,
				status : this.user.status,
				job : this.job
		
			};
			
			this.http.post(
				Constants.BASEURL + 'rest/client',
				json,
				{ withCredentials : true } )
			.toPromise()
			.then( (response)=>{
				//this.router.navigate(['dashboard']);
				$('#successModal').modal('show');
				this.user.id = response.json().data.clientId;
				
			})
			.catch( ( response )=>{
				let json = response.json();
				if (json.errorCause == "userAlreadyExists") {
					this.errors.username = 'Már foglalt felhasználónév!';
				}
			
			});
		} else {
			this.applicationRef.tick();	//ha lefuttata a change detectiont és frissíti a domot, akkor utána megy tovább
			var formGroup = document.getElementsByClassName("form-group");
			for (var i = 0; formGroup.length > i; i++) {
				if (formGroup[i].classList.contains("has-error") ) {
					let rect = formGroup[i].getBoundingClientRect();									
					window.scrollBy (0, rect.top );
					break;
				}			
			}
		}	
	}
	hasRole( roleName : string) : boolean {
        let t : boolean = false;
        for (let role of this.currentUser.role) {
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    }
	onBlurMethodName() :void {
		if (this.userService.wordLengthValidator(this.user.name, 1)) {
			delete this.errors.name;
		}	

	}
	onBlurMethodEmail() :void {
		if (this.userService.emailValidator(this.user.email)) {
			delete this.errors.email;
		}
	}
	onBlurMethodPhone() :void {
		if (this.userService.phoneNumberValidator(this.user.phone)) {
			delete this.errors.phone;
		}
	}
	onBlurMethodGender() :void {
		if (this.userService.wordLengthValidator(this.user.gender, 1)) {
			delete this.errors.gender;
		}
	}
}
