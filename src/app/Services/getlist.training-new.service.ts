import {Injectable} from "@angular/core";
import { Http, HttpModule, Response, Headers,} from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../constant';
import { Address, Location, Course, CourseEvent, User } from '../00_commons/interface';
import { URLSearchParams } from "@angular/http";

//let headers = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class NewTrainingService{



  trainingRequest: string ="rest/courses";
  addCourseRequest: string = "rest/courses/types";

	constructor(private http: Http) {}

  listTypes(): Promise<Response>{
    // Promise<Response> a Responsive kell bele
    return this.http.get(Constants.BASEURL+"rest/courses/types",{ withCredentials : true })
		.toPromise();      
  }
  /*
  getStartingCourseList() :Promise<Course[]> {
    return this.http.get(
      Constants.BASEURL+"rest/valami",
      {withCredentials: true})
    .toPromise()
    .then((response)=>{
      return response.json() as Course[];
    })
    .catch( this.handleError );
  }
*/
  getFilteredCourseList( searched:string ) :Promise<Course[]> {
    return this.http.get(
      Constants.BASEURL+"rest/courses?searchexpression=" + searched,
      {withCredentials: true})
    .toPromise()
    .then((response)=>{
      return response.json() as Course[];
    })
    .catch( this.handleError );
  }
  /*
  getEveryCourseList() :Promise<Course[]> {
    return this.http.get(
      Constants.BASEURL+"rest/courses/all",
      {withCredentials: true})
    .toPromise()
    .then((response)=>{
      return response.json() as Course[];
    })
    .catch( this.handleError );
  }
  */
  handleError(error: any): Promise<any> {
      console.error('HIBA', error.status); // for demo purposes only
      return Promise.reject(error.message || error);
  }


  listGetTrainers() : Promise<Response>{
  	return this.http.get(Constants.BASEURL+"rest/trainers", {withCredentials : true})
  		.toPromise();
  }

  postTrainingRequest (postObject: Object) : Promise<Response>{

  	return this.http.post(Constants.BASEURL + this.trainingRequest, postObject,{ withCredentials : true })
  		.toPromise();
  }
  postTrainingRequestPut (postObject: Object) : Promise<Response>{

    return this.http.put(Constants.BASEURL + this.trainingRequest, postObject,{ withCredentials : true })
      .toPromise();
  }

  TimeValidator (trainingDateFinishTime : string) : boolean {
    let timevalid = /(?:(?:(?:(?<hh>\d{1,2})[:.])?(?<mm>\d{1,2})[:.])?(?<ss>\d{1,2})[:.])?(?<s>\d{1,2})/;
    
    return timevalid.test(trainingDateFinishTime );
  }

  getTrainingData(idText : string) : Promise<Course> {
       return this.http.get(
      Constants.BASEURL+"rest/coursesbyid?courseId=" + idText,
      {withCredentials: true})
    .toPromise()
    .then( (response) => {
      let json = response.json();
      if (json.errorCode && json.errorCode == "NotValid") {
        return new Promise<Course>((resolve, reject) => { reject(); });
      } else {
        return json as Course;
      }
    })
    .catch(this.handleError);
 
  }

  courseTypePush(coursePush: Object) : Promise<Response> {
    return this.http.post(Constants.BASEURL + this.addCourseRequest, coursePush,{ withCredentials : true })
      .toPromise();
  }

  getcomplexSearch() :Promise<NewTrainingService> {
    return //this.http.get("/rest/courses?type=<valami>&name=<valami>&dateStartBefore=<1800-01-01>&dateStartAfter=<2100-01-01>&dateFinishBefore=<1900-01-01>&dateFinishAfter=<2013-01-01>&location=<Rom>")
    //.toPromise();
  }

  listId(id: string) : Promise<Response>{
    return this.http.get(Constants.BASEURL+"rest/coursesbyid?courseid=" + id, {withCredentials : true})
    .toPromise();
  }
  /*
   postTrainingRequest (course: Course) : Promise<Response>{
    return this.http.post(Constants.BASEURL + this.trainingRequest,JSON.stringify(Response,{ withCredentials : true })
      .toPromise();
  }
  */


/*
  getName(name:string): Promise<Response>{
    return this.http.get(Constants.BASEURL+"")
      .toPromise();      
  }

  getDescription(description:string): Promise<Response>{
        return this.http.get(Constants.BASEURL+"")
      .toPromise();      
  }

/*
	create(name: string): Promise<Hero> {
    return this.http
      .post(this.heroesUrl, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Hero)
      .catch(this.handleError);
  }

  update(hero: Hero): Promise<Hero> {
    const url = `${this.heroesUrl}/${hero.id}`;
    return this.http
      .put(url, JSON.stringify(hero), {headers: this.headers})
      .toPromise()
      .then(() => hero)
      .catch(this.handleError);
  }
*/


}
