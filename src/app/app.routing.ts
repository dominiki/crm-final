import { RouterModule, Routes} from '@angular/router';

import { LoginComponent } from './01_login/login.component';
import { ForgottenPwComponent } from './01_forgottenpassword/forgotten-pw.component';
import { RegistrationComponent } from './01_registration/registration.component';
import { RegistrationComponent2 } from './01_registration/registration2.component';
import { SuccessReg } from './01_registration/success-reg.component';
import { TrainingNewComponent } from './02_training_new/training-new.component';
import { UsersComponent } from './02_users/users.component';
import { ClientsComponent } from './02_clients/clients.component';
import { SetupComponent } from './02_setup/setup.component';
import { DashboardComponent } from './02_dashboard/dashboard.component';
import { ProfileComponent } from './03_profile/profile.component';
import { MapComponent } from './03_map/map.component';
import { TrainingsComponent } from './02_trainings/trainings.component';
import { UserDetailsComponent } from './03_UserDetails/userDetails.component';
import { ClientDetailsComponent } from './02_clientDetails/clientDetails.component';
import {TrainingDetails} from './02_trainingDetails/trainingDetails.component';



const appRoutes: Routes = [

	{ path:'', redirectTo:'/login', pathMatch:"full"},	
	{ path: 'login', component: LoginComponent },
	{ path: 'forgotten-pw', component: ForgottenPwComponent },
	{ path: 'registration', component: RegistrationComponent },
	{ path: 'registration/:id', component: RegistrationComponent2 },
	{ path: 'successreg', component: SuccessReg },
	{ path: 'training-new', component: TrainingNewComponent },
	{ path: 'users', component: UsersComponent },
	{ path: 'clients', component: ClientsComponent },
	{ path: 'setup', component: SetupComponent },
	{ path: 'user/:id', component:  UserDetailsComponent },
	{ path: 'client/:id', component:  ClientDetailsComponent },
	{ path: 'dashboard', component: DashboardComponent },
	{ path: 'profile', component: ProfileComponent },
	{ path: 'map', component: MapComponent },
	{ path: 'trainings', component: TrainingsComponent },
	{ path: 'trainingDetails/:course.courseId', component: TrainingDetails },
	

];

export const routes = RouterModule.forRoot(appRoutes);