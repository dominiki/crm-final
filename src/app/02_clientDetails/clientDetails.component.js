"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var interface_1 = require("../00_commons/interface");
var user_service_1 = require("../services/user.service");
var getlist_training_new_service_1 = require("../services/getlist.training-new.service");
var message_service_1 = require("../services/message.service");
require("rxjs/add/operator/toPromise");
var ClientDetailsComponent = (function () {
    function ClientDetailsComponent(http, router, userService, courseService, messageService, activatedRoute) {
        this.http = http;
        this.router = router;
        this.userService = userService;
        this.courseService = courseService;
        this.messageService = messageService;
        this.activatedRoute = activatedRoute;
        this.user = {
            name: "",
            email: "",
            isActive: false,
            id: this.activatedRoute.snapshot.params['id'],
            gender: "",
            role: "",
            username: "",
            phone: "",
            address: [{ zip: "", city: "", street: "", number: "" }],
            birthDate: "",
            birthPlace: ""
        };
        this.currentUser = {
            name: "",
            email: "",
            isActive: false,
            id: null,
            role: ""
        };
        this.genders = interface_1.genders;
        this.errors = {};
        this.locations = [{ zip: "", city: "", street: "", number: "", other: "" }];
    }
    ClientDetailsComponent.prototype.ngOnInit = function () {
        /*		document.body.className = "profile";
                console.log( this.activatedRoute.snapshot.params['id']);
                this.userService.getGuestData(this.user.id)
                    .then( user => this.user = user );
                this.userService.getCurrentUser().then( user => this.currentUser = user );
                this.userService.getAddresses().then( addresses => this.locations = addresses.addresses );
                $( "#datepicker" ).datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeYear:true,
                    yearRange: '1910:2017'
                });
        */
    };
    ClientDetailsComponent.prototype.changeData = function () {
        //ide jön a módosítás, amit szeretnénk elküldeni json-ként...Balázs csinálja a backendjét.
    };
    ClientDetailsComponent.prototype.hasRole = function (roleName) {
        var t = false;
        for (var _i = 0, _a = this.currentUser.role; _i < _a.length; _i++) {
            var role = _a[_i];
            if (role == roleName) {
                t = true;
                break;
            }
        }
        return t;
    };
    ClientDetailsComponent.prototype.deleteData = function () {
    };
    return ClientDetailsComponent;
}());
ClientDetailsComponent = __decorate([
    core_1.Component({
        selector: 'client',
        templateUrl: './clientDetails.component.html'
    }),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router, user_service_1.UserService, getlist_training_new_service_1.NewTrainingService, message_service_1.MessageService, router_1.ActivatedRoute])
], ClientDetailsComponent);
exports.ClientDetailsComponent = ClientDetailsComponent;
//# sourceMappingURL=clientDetails.component.js.map