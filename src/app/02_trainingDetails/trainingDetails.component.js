"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var getlist_training_new_service_1 = require("../services/getlist.training-new.service");
require("rxjs/add/operator/toPromise");
var user_service_1 = require("../services/user.service");
var TrainingDetails = (function () {
    function TrainingDetails(newTrainingService, userService, activatedRoute) {
        this.newTrainingService = newTrainingService;
        this.userService = userService;
        this.activatedRoute = activatedRoute;
        // itt vannak a promisok
        //@Input()
        this.trainingEvents = [];
        this.address = {
            zip: null,
            city: "",
            street: "",
            number: ""
        };
        this.location = {
            address: this.address,
            room: ""
        };
        this.training = {
            name: "",
            eventId: null,
            description: "",
            teachers: [],
            location: this.location,
            dateStart: "",
            dateFinish: ""
        };
        this.errorText = "";
        // this.genders = genders;
        this.errors = {};
        this.newTrainingServiceInstance = newTrainingService;
        this.postText = "";
        this.trainingDateStartDate = "";
        this.trainingDateFinishTime = "";
        this.sub = "";
        this.id = "";
        this.courseTypes = new Array();
        this.trainersList = new Array();
        this.courseEvents = new Array();
        this.courseIdGet = new Array();
        this.addCourseType = new Array();
        this.backCopy = new Array();
        this.course = {
            courseName: "",
            courseDescription: "",
            courseId: this.activatedRoute.snapshot.params['course.id'],
            applicationDeadline: "",
            maxStudents: null,
            minStudents: null,
            courseDateStart: "",
            courseDateFinish: "",
            discount: [],
            price: null,
            priceDeadline: "",
            events: this.trainingEvents,
        };
    }
    TrainingDetails.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.activatedRoute.params.subscribe(function (course) {
            _this.id = course['course.courseId'];
        });
        console.log(this.id);
        document.body.className = "page-container-bg-solid";
        $("#datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            yearRange: '1910:2017',
            changeYear: true
        });
        this.getTypes();
        this.getTrainers();
        this.getCourseById();
        /* console.log( this.activatedRoute.snapshot.params['course.id']);
        this.newTrainingService.getTrainingData(this.user.id)
        .then( course => this.course = course );*/
    };
    TrainingDetails.prototype.addCourseEvents = function () {
        if (this.training.teachers == []) {
            var nullTeacher = Array();
            nullTeacher.push();
        }
        var trainingCopy = JSON.parse(JSON.stringify(this.training));
        this.trainingEvents.push(trainingCopy);
    };
    TrainingDetails.prototype.deleteCourseEvents = function (index) {
        this.trainingEvents.splice(index, 1);
    };
    TrainingDetails.prototype.getTypes = function () {
        var _this = this;
        this.newTrainingServiceInstance.listTypes()
            .then(function (response) {
            _this.courseTypes = response.json();
        })
            .catch(function (response) {
            console.log(response);
            _this.errorText = "Hiba van a types listában!";
        });
    };
    TrainingDetails.prototype.getTrainers = function () {
        var _this = this;
        this.newTrainingServiceInstance.listGetTrainers()
            .then(function (trainers) {
            _this.trainersList = trainers.json();
            _this.trainerNameGet = trainers.json();
        })
            .catch(function (trainers) {
            _this.errorText = "Hiba van a trainers litában";
        });
    };
    TrainingDetails.prototype.trainingRequest = function () {
        /*
            let courseEvents= new Array<Object>();
            this.course.events.forEach((event) => {
              let trainers = new Array<string>();
              event.teachers.forEach((teacher) => {
                 trainers.push(teacher.id);
              });
              let typespush = new Array<string>();
              this.course.types.forEach((types) =>{
                  typespush.push(types);
                  });
              
        
              courseEvents.push({
                name: event.name,
                courseEventDescription: event.description,
                dateStart: event.dateStart,
                dateFinish: event.dateFinish,
                trainers: trainers,
                location: event.location
              });
              
            });
            let requestObj = {
              name: this.course.courseName,
              description: this.course.courseDescription,
              discount: this.course.discount,
              maxStudents: ""+this.course.maxStudents,
              minStudents: ""+this.course.minStudents,
              applicationDeadline: this.course.applicationDeadline,
              price: ""+this.course.price,
              courseEvents: courseEvents
             };
           
        
            this.newTrainingServiceInstance.postTrainingRequestPut (requestObj)
            .then(response =>
              alert("A " + this.course.courseName+" kurzus sikeresen rögzítve lett!")
            )
            .catch( ( response )=>{
              try{
                console.log(response);
                let json = response.json();
                this.errorText = "Hiba van a trainer post-ban!";
              }
              catch (e) {
              this.errorText = "Hiba van a trainer post-ban!";
              }
        
            });*/
    };
    // getCourseById( this.activatedRoute.snapshot.params['course.id']) : void {
    TrainingDetails.prototype.getCourseById = function () {
        var _this = this;
        this.newTrainingServiceInstance.listId(this.id)
            .then(function (response) {
            _this.courseIdGet = response.json();
            _this.backCopy = _this.courseIdGet;
        })
            .catch(function (response) {
            console.log(response);
            _this.errorText = "Hiba van a types listában!";
        });
    };
    TrainingDetails.prototype.addCourseTypeButton = function () {
        var _this = this;
        var coursePush = new Array();
        coursePush.push({
            text: this.addCourseType,
        });
        this.newTrainingServiceInstance.courseTypePush(coursePush)
            .then(function () {
            //this.router.navigate(['dashboard']);
            _this.getTypes();
        })
            .catch(function (response) {
            try {
                console.log(response);
                var json = response.json();
                _this.errorText = "Hiba van a trainer post-ban!";
            }
            catch (e) {
                _this.errorText = "Hiba van a trainer post-ban!";
            }
        });
    };
    TrainingDetails.prototype.backToCourseEvent = function () {
        $("html, body").animate({ scrollTop: $(document).height() - $(window).height() }, 'slow');
    };
    TrainingDetails.prototype.backToCourse = function () {
        $("html, body").animate({ scrollTop: "260px" }, 'slow');
    };
    return TrainingDetails;
}());
TrainingDetails = __decorate([
    core_1.Component({
        selector: 'training-new',
        //selector: 'trainingDetails',
        templateUrl: './trainingDetails.component.html',
        providers: [getlist_training_new_service_1.NewTrainingService]
    }),
    __metadata("design:paramtypes", [getlist_training_new_service_1.NewTrainingService, user_service_1.UserService, router_1.ActivatedRoute])
], TrainingDetails);
exports.TrainingDetails = TrainingDetails;
//# sourceMappingURL=trainingDetails.component.js.map