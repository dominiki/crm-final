export interface KeyValueObj {
	key : string,
	value : any
}

export const genders : KeyValueObj[] = [
	{ key : 'MALE', value : 'Férfi' },
	{ key : 'FEMALE', value : 'Nő' }
];

export interface Address {
	zip?: number;
	city?: string;
	id?: string;
	street?: string;
	number?: string; //mert lehet például: 154b
	other?: string;	
};

export interface Location {
	address?: Address;
	room?: string;
	selected?: boolean;
};

export interface CourseEvent {	//ezek a kurzuson belüli tréningek
	name: string;	
	eventId?: number;
	description: string;
	teachers?: User [];
	location?: Location;
	dateStart?: string;
	dateFinish?: string;
};

export interface Discount {
	discountName?: string;
	discountAmount?: number;
}

export interface Course {	//Ha csak 1 CourseEvent van, akkor is egy kurzus része lesz
  	courseName: string;
 	courseDescription: string;
  	courseId?: number;
  	applicationDeadline?: string;
  	maxStudents?: number;
  	minStudents?: number;
  	courseDateStart?: string;	//csak lekérésként fogjuk látni a courseEventből
	courseDateFinish?: string;	//csak lekérésként fogjuk látni a courseEventből
  	discount?: Discount[];
	price?: number;			//TODO még a tagsági díjjat ki kell találni, hogy legyen
	priceDeadline?: string;
	events?: CourseEvent[];
 	courseTypes?: string[];	//ezek lesznek a tagek (setupban lehet kezelni)
 	selected?: boolean;	//ez csak a selectes html részeknél kell az nGmodelhez
};

export interface UserFee {	//TODO ezt a részt is véglegesíteni kell a tagsági díjjal együtt
	id?: string;		
	paidFee?: number;
	feeExpiry?: string;
}

export interface UserSearch {
	users: any;
}

export interface User {
	name: string;
	email: string;
	isActive: boolean;
	id?: number;	//első szám: 1-user, 2-client, 3-course, 4-courseEvent; A második szám pedig a sorszám, ami mindig 1-ről indul, csak a beégetett adminé 0
	gender?: string;
	birthPlace?: string;
	birthDate?: string;
	phone?: string;
	address?: Address;
	role?: string;
	username?: string;
	password?: string;	//TODO később HASHelés
	subjects?: string [];	// setupban lehet kezelni
	locations?: Address [];
	actCourses?: Course [];	
	appliedCourses?: Course [];	
	previousCourses?: Course [];
	otherInformation?: string;
	status?: string;
	fee?: UserFee[];
};

export interface SearchFilter {
}
export interface ClientSimpleSearchFilter extends SearchFilter {
	s : string;
}
export interface ClientCombinedSearchFilter extends SearchFilter {
	name : string;
	email : string;
	status : string;
}
export interface TableHeader {
	text : string;
	align?: string;
}
export interface SearchResult {
	data : any[];
	pageCount : number;
}