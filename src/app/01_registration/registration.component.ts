import { Component, OnInit, ApplicationRef, NgModule, Injectable } from '@angular/core';
import { Http, HttpModule, Response, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';

import { Address, Location, Course, CourseEvent, User, KeyValueObj } from '../00_commons/interface';
import { VariableSetup } from '../00_commons/setup-constans';
import { Constants } from '../constant';
import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';
import 'rxjs/add/operator/toPromise';

declare var $ : any;

@Component({
  selector: 'registration',
  templateUrl: './registration.component.html'
})
export class RegistrationComponent implements OnInit { 

	user : User;
	invExp : number;
	invText : string;
	data : any;
	roles : string;
	errors : any;

	constructor( private http: Http, private applicationRef : ApplicationRef, private messageService:MessageService, private userService:UserService, private router: Router ){
		this.user = {
			email: "",
			name: "",
			isActive: false,
			role: ""
		};
		this.roles = "";
		this.invExp = VariableSetup.getInvExp();
		this.invText = VariableSetup.getInvText();
		this.errors = {};
	}

	ngOnInit() : void {
		document.body.className = "profile";
		$('#registrerSuccessModal').modal({ show : false });
	}

	doSubmit() : void {
		this.errors = {};
		if (!this.userService.wordLengthValidator(this.user.name, 1)) {
			this.errors.name = 'Kérem adja meg a nevét!';
		}
		if (!this.userService.emailValidator(this.user.email)) {
			this.errors.email = 'Érvénytelen email formátum!';
		}
		if (!this.user.role[0]) {
			this.errors.role = 'Kérem adja meg az ügyfél típusát!';
		}
		if (!this.userService.wordLengthValidator(this.invExp+"", 1)) {
			this.errors.invExp = 'Kérem adja meg a meghívó érvényességét!';
		}

		if (Object.keys(this.errors).length == 0) {
			let data = {
				name : this.user.name,
				email : this.user.email,
				role : this.user.role,
				text : this.invText,
				expiry : this.invExp
			};

			this.http.post(
				Constants.BASEURL + 'rest/invitation',
				data,
				{ withCredentials : true } )
			.toPromise()
			.then( (response)=>{
				let json = response.json();
				if (json.status == "ok") {
					$('#registrerSuccessModal').modal('show').on('hidden.bs.modal', () =>{
						this.router.navigate(['dashboard']);
					});
				}
			})
			.catch( ( response )=>{
				this.messageService.sendMessage("Szerverhiba", "A szerver nem várt választ adott: " + response.error);
			});
		} else {
			this.applicationRef.tick();	
			var formGroup = document.getElementsByClassName("form-group");
			for (var i = 0; formGroup.length > i; i++) {
				if (formGroup[i].classList.contains("has-error") ) {
					let rect = formGroup[i].getBoundingClientRect();									
					window.scrollBy (0, rect.top );
					break;
				}			
			};
		}
	}    

	onBlurMethodName() :void {
		if (this.userService.wordLengthValidator(this.user.name, 1)) {
			delete this.errors.name;
		}	
	}

	onBlurMethodEmail() :void {
		if (this.userService.emailValidator(this.user.email)) {
			delete this.errors.email;
		}
	}

	onBlurMethodRole() :void {
		if (this.userService.wordLengthValidator(this.user.role[0], 1)) {
			delete this.errors.role;
		}
	}

	onBlurMethodInvExp() :void {
		if (this.user.role[0]) {
			delete this.errors.invExp;
		}
	}

}