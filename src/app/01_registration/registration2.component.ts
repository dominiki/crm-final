import { Component, OnInit, NgModule, Injectable, ApplicationRef } from '@angular/core';
import { Http, HttpModule, Response, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

import { Address, Location, Course, CourseEvent, genders, KeyValueObj, User } from '../00_commons/interface';
import { VariableSetup } from '../00_commons/setup-constans';
import { Constants } from '../constant';
import { UserService } from '../services/user.service';
import { MessageService } from '../services/message.service';
import 'rxjs/add/operator/toPromise';


@Component({
	selector: 'registration2',
	templateUrl: './registration2.component.html'
})
export class RegistrationComponent2 implements OnInit { 

	user : User;
	genders : KeyValueObj[];
	locations : Location [];
	password1 : string;
	password2 : string;
	errors : any;
	rulesText : string;
	rulesChecked : boolean;

	constructor( private http: Http, private router: Router, private userService:UserService, private activatedRoute : ActivatedRoute, private applicationRef : ApplicationRef, private messageService : MessageService){
		this.rulesText = VariableSetup.getIssueText();
		this.rulesChecked = false;
		this.locations = [
			{ address : {} , selected : true}
		];
		this.user = {
			name: "",
			email: "",
			isActive: false,
			id: this.activatedRoute.snapshot.params['id'],
			gender: "",
			role: "",
			username: "",
			phone: "",
			address : { zip : null, city : "", street : "", number : "", other : "" }
		};
		this.genders = genders;
		this.password1 = "";
		this.password2 = "";
		this.errors = {};
	}

	ngOnInit() : void {
		document.body.className = "profile";
		//console.log( this.activatedRoute.snapshot.params['id'] );
/*		this.userService.userFromRegistrationId(this.user.id)
		.then( getUser => {
			Object.assign(this.user, getUser.user);
			if (this.hasRole("ROLE_TRAINER")) {
				this.locations = getUser.locations;
			}
		} )
		.catch( () => {
			this.router.navigate(['/']);
		} );*/
	}

	doRegister() : void {
		this.errors = {};
		if (!this.userService.wordLengthValidator(this.user.name, 1)) {
			this.errors.name = 'Kérem adja meg a nevét!';
		}
		if (!this.userService.wordLengthValidator(this.user.gender, 1)) {
			this.errors.gender = 'Kérem adja meg a nemét!';
		}
		if (!this.userService.phoneNumberValidator(this.user.phone)) {
			this.errors.phone = 'Érvénytelen telefonszám, kérem adja meg a telefonszámot a következő módon: "+3611234567"!';
		}
		if (!this.userService.wordLengthValidator(this.user.username, 6)) {
			this.errors.username = 'Nem megfelelő felhasználónév! (legalább 6 karakter)';
		}
		if (this.password1 != this.password2) {
			this.errors.password2 = 'Nem egyezik a 2 jelszó!';
		}
		if (!this.userService.passwordValidator1(this.password1)) {
			this.errors.password1 = 'Nem megfelelő jelszóformátum! (legalább 6 karakter, legyen benne kisbetű, nagybetű és szám)';
		}
		if (!this.userService.passwordValidator1(this.password2)) {
			this.errors.password2 = 'Nem megfelelő jelszóformátum! (legalább 6 karakter, legyen benne kisbetű, nagybetű és szám)';
		}

		if (Object.keys(this.errors).length == 0) {

			let postUser = JSON.parse(JSON.stringify(this.user));
			postUser.password = this.password1;
			postUser.generatedId = postUser.id;
			delete postUser.id;
			postUser.telephone = postUser.phone;
			delete postUser.phone;
			if (this.hasRole("ROLE_TRAINER")) {   
				var notAcceptedLocations : any;
				for (var loc of this.locations){
					if (! loc.selected) {
						notAcceptedLocations.append(loc.address.id);
					}
				}
				postUser.locations = notAcceptedLocations;
			}
		
			this.http.post(
				Constants.BASEURL + 'rest/user',
				postUser,
				{ withCredentials : true } )
			.toPromise()
			.then( ()=>{
				this.router.navigate(['successreg']);
			})
			.catch( ( response )=>{
				try {
					let json = response.json();
					if (json.errorCause == "userAlreadyExists") {
						this.errors.username = 'Már foglalt felhasználónév!';
					} else {
						this.messageService.sendMessage("Szerverhiba", "A szerver nem várt választ adott: " + response.error);
					}
				} catch( e ) {
					this.messageService.sendMessage("Szerverhiba", "A szerver nem várt választ adott: " + response.error);
				}
			});

		} else {
			this.applicationRef.tick();	//ha lefuttata a change detectiont és frissíti a domot, akkor utána megy tovább
			var formGroup = document.getElementsByClassName("form-group");
			for (var i = 0; formGroup.length > i; i++) {
				if (formGroup[i].classList.contains("has-error") ) {
					let rect = formGroup[i].getBoundingClientRect();									
					window.scrollBy (0, rect.top );
					break;
				}			
			}
		}
	}

	hasRole( roleName : string) : boolean {
		let t : boolean = false;
		for (let role of this.user.role) {
			if (role == roleName) {
				t = true;
				break;
			}
		}
		return t;
	}

	onBlurMethodName() :void {
		if (this.userService.wordLengthValidator(this.user.name, 1)) {
			delete this.errors.name;
		}	
	}

	onBlurMethodGender() :void {
		if (this.userService.wordLengthValidator(this.user.gender, 1)) {
			delete this.errors.role;
		}
	}

	onBlurMethodPhone() :void {
		if (this.userService.phoneNumberValidator(this.user.phone)) {
			delete this.errors.phone;
		}	
	}

	onBlurMethodUserName() :void {
		if (this.userService.wordLengthValidator(this.user.username, 6)) {
			delete this.errors.username;
		}
	}

	onBlurMethodPass1() :void {
		if (this.userService.passwordValidator1(this.password1)) {
			delete this.errors.password1;
		}
	}

	onBlurMethodPass2() :void {
		if (this.userService.passwordValidator1(this.password2)) {
			delete this.errors.password2;
		}
	}
}