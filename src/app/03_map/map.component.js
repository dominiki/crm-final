"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var MapComponent = (function () {
    function MapComponent(activatedRoute) {
        this.activatedRoute = activatedRoute;
    }
    MapComponent.prototype.ngOnInit = function () {
        this.activatedRoute.fragment.subscribe(function (f) {
            var element = document.querySelector("#" + f);
            if (element)
                element.scrollIntoView(element);
        });
        document.body.className = "";
        var mapProp11 = {
            center: new google.maps.LatLng(47.50768051597303, 19.02868439150446),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };
        var mapProp12 = {
            center: new google.maps.LatLng(47.48729549999999, 19.0663021),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };
        var mapProp2 = {
            center: new google.maps.LatLng(46.249757, 20.143825),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };
        var mapProp3 = {
            center: new google.maps.LatLng(47.6833564, 17.6339528),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };
        var map1 = new google.maps.Map(document.getElementById("googleMap11"), mapProp11);
        var map2 = new google.maps.Map(document.getElementById("googleMap12"), mapProp12);
        var map3 = new google.maps.Map(document.getElementById("googleMap2"), mapProp2);
        var map4 = new google.maps.Map(document.getElementById("googleMap3"), mapProp3);
        var marker1 = new google.maps.Marker({ position: mapProp11.center, map: map1 });
        var marker2 = new google.maps.Marker({ position: mapProp12.center, map: map2 });
        var marker3 = new google.maps.Marker({ position: mapProp2.center, map: map3 });
        var marker4 = new google.maps.Marker({ position: mapProp3.center, map: map4 });
    };
    return MapComponent;
}());
MapComponent = __decorate([
    core_1.Component({
        selector: 'map',
        templateUrl: './map.component.html',
        styleUrls: ['../../../assets/css/map.component.css']
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute])
], MapComponent);
exports.MapComponent = MapComponent;
//# sourceMappingURL=map.component.js.map